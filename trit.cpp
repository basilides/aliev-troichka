#include "trit.h"

Trit::Trit()
{
}

Trit::Trit(char t)
{
    if(t == '0') data = 1;
    if(t == 'T') data = 0;
    if(t == '1') data = 2;
}

Trit::Trit(const Trit &tr)
{
    data = tr.data;
}

Trit::Trit(ushort n)
{
    if(n == 3) data = 2;
    else data = n;
}

void Trit::set(ushort t)
{
    data = t;
}

void Trit::operator =(ushort t)
{
    data = t;
}

bool Trit::operator == (const Trit &s)
{
    if(s.data == data) return true;
    return false;
}

Trit Trit::reverse () const
{
    if(toChar () == 'T') return Trit('1');
    if(toChar () == '1') return Trit('T');
    return Trit('0');
}


Trit Trit::mult (const Trit &s)
// умножаем это вот число на s
{
    if(s.toChar () == '0' || toChar () == '0') return Trit('0');
    if(s.toChar () == '1') return Trit(data);
    if(toChar() == '1') return Trit(s);
    if(s.toChar () == 'T') return Trit('1');
    exit(1111);
}

Trit Trit::min (const Trit &s, Trit *o)
{
    int r = toInt () - s.toInt () + o->toInt () ; // вычитаем из этого вот Другое и прибавляем(!) остаток

   // std::cout << "r(MIN) = " << r << std::endl;
    if(r == 3)
    {
        *o = Trit((ushort)2);
        return Trit((ushort)1);
    }
    if(r == 2)
    {
        *o = Trit((ushort)2);
        return Trit((ushort)0);
    }

    if(r == -2)
    {
        *o = Trit((ushort)0);
        return Trit((ushort)2);
    }

    if(r == -3)
    {
        *o = Trit((ushort)0);
        return Trit((ushort)1);
    }

    *o = Trit((ushort)1);
    return Trit((ushort)(r+1));
}

Trit Trit::sum (const Trit &s, Trit *o)
{
    int r = s.toInt () + toInt () + o->toInt ();
    //std::cout << "r = " << r << std::endl;
    if(r == 3)
    {
        *o = Trit((ushort)2);
        return Trit((ushort)1);
    }
    if(r == 2)
    {
        *o = Trit((ushort)2);
        return Trit((ushort)0);
    }



    if(r == -2)
    {
        *o = Trit((ushort)0);
        return Trit((ushort)2);
    }

    if(r == -3)
    {
        *o = Trit((ushort)0);
        return Trit((ushort)1);
    }

    *o = Trit((ushort)1);
    return Trit((ushort)(r+1));
}

char Trit::toChar () const
{
    if(data == 0) return 'T';
    if(data == 1) return '0';
    return '1';
}

int Trit::toInt () const
{
    if(data == 0) return -1;
    if(data == 1) return 0;
    return 1;
}
