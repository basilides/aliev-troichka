#ifndef TRIT_H
#define TRIT_H
typedef unsigned short int ushort;
#include <iostream>
class Trit
{
public:
    Trit();
    Trit(char t);
    Trit(ushort n);
    Trit(const Trit & tr);

    void set(ushort t);

    void operator =(ushort);
    bool operator == (const Trit &s);

    // атомарные операции; не знаю, куда деть остаток; можно возвращать в структуре, но, в принципе
    // "возвращение грязью" я видел в кьюте и ничего плохого в этом не вижу, если работает на
    // коротком и легкоконтролируемом участке кода.
    Trit min(const Trit &s, Trit *o);
    Trit sum(const Trit & s, Trit * o);
    Trit mult(const Trit &s);

    Trit reverse() const;

    char toChar() const;
    int toInt() const;

private:
    ushort data;
};

#endif // TRIT_H
