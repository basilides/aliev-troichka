#-------------------------------------------------
#
# Project created by QtCreator 2014-03-24T03:59:47
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = troichka
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    trit.cpp \
    trint.cpp

QMAKE_CXXFLAGS += -std=c++0x

OTHER_FILES += \
    todo.txt

HEADERS += \
    trit.h \
    trint.h
