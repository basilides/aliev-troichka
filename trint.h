#ifndef TRINT_H
#define TRINT_H

#include <trit.h>
#include <QList>

class trInt
{
public:
    trInt();
    trInt(int n);

    trInt& operator =(const trInt &s);
    trInt operator + (const trInt & s_);
    trInt operator - (const trInt & s_);
    trInt operator *(const trInt & s_);
    trInt operator /(const trInt &s_);
    bool operator==(const trInt &s);
    trInt& operator +=(const trInt &s);



    trInt operator ++();
    trInt operator --();

    Trit getAt(int i) const;

    void resign();

    double toDouble() const;

    void show();

    int digits() const;

private:
    trInt chop(int n);
    trInt(QList<Trit> d);
    QList<Trit> data; // очень некрасивое и плохое решение
};


#endif // TRINT_H
