#include "trint.h"

trInt::trInt()
{
}

trInt::trInt(QList<Trit> d)
{
    for(Trit d_ : d)
        data.push_back (d_);
}


trInt::trInt(int n)
{
    if(n == 0) data.push_back (Trit((ushort)1)); // и еще один костыль
    else
    {

        bool ltn = 0;
        if(n < 0)
        {
            ltn = 1;
            n *= -1;
        }
        Trit b;
        while(n > 1)
        {
            int a = n%3;
            if(a == 0) b = 1;
            if(a == 1) b = 2;
            if(a == 2) b = 0;
            data.push_front (b);
            n = n/3; if(a == 2) n++;
        }
        
        data.push_front (Trit((ushort)2));

        if(ltn) resign(); // тоже некрасиво и криво
    }
}




trInt& trInt::operator =(const trInt &s)
{
    data.clear ();

    for(Trit t : s.data)
        data.push_back (t);

    return *this;
}

trInt&
trInt::operator += (const trInt &s)
{
    *this = *this + s;
    return *this;
}

trInt trInt::operator + (const trInt &s_)
{
    trInt s = s_;
    trInt t = *this;

    trInt result;
    Trit ost((ushort)1);

    if(s.digits () > t.digits ())
    {
        s.show (); t.show ();
        trInt buf = s;
        s = t;
        t = buf;
        //std::cout << "WORKED" << std::endl;
        s.show (); t.show ();
    }

    // теперь t - это то, что было this, и оно всегда больше, чем s

    // мы складываем конец второго с первым
    int c = t.digits () - s.digits ();
    //std::cout << "delta = " << c << std::endl;
    for(int i = t.digits (); i >= c; i--)
    {
        result.data.push_front (t.data[i].sum (s.data[i - c], &ost));
    }

    // теперь осталось досложить первое с нулем

    if(c != 0)
        for(int i = c - 1; i >= 0; i--)
            result.data.push_front (t.data[i].sum(Trit(ushort(1)), &ost));
    if(ost.toInt () == 1) result.data.push_front (Trit(ushort(2)));
    if(ost.toInt () == -1) result.data.push_front (Trit(ushort(0)));
    return result;
}

trInt trInt::operator - (const trInt &s_)
{
    trInt s = s_;
    trInt t = *this;

    trInt result;
    Trit ost((ushort)1);

    if(s.digits () > digits ())
    {
        trInt b = t;
        t = s;
        s = b;
    }

    // теперь t - это то, что было this, и оно всегда больше, чем s

    // вычитаем конец второго из первого
    int c = t.digits () - s.digits ();
   // std::cout << "delta = " << c << std::endl;
    for(int i = t.digits (); i >= c; i--)
    {
        result.data.push_front (t.data[i].min (s.data[i - c], &ost));
    }

    // теперь осталось довычесть первое с нулями

    if(c != 0)
        for(int i = c - 1; i >= 0; i--)
            result.data.push_front (t.data[i].min(Trit(ushort(1)), &ost));
    if(ost.toInt () == 1) result.data.push_front (Trit(ushort(2)));
    if(ost.toInt () == -1) result.data.push_front (Trit(ushort(0)));
    return result;
}

trInt trInt::operator * (const trInt &s)
{
    // выполняем умножение одного на другое, а потом складываем со сдвигом
    // я не парился и сделал "в столбик"
    trInt result(0);
    trInt trint_buf;
    Trit buf;
    int n = -1;
    for(int i = s.digits (); i >= 0; i--)
    {
        for(int j = digits (); j >= 0; j--)
        {
            trint_buf.data.push_front (Trit(s.data[i]).mult (Trit(data[j])));
        }
        for(int k = 0; k <= n; k++) trint_buf.data.push_back (Trit('0'));
        //std::cout << "DEBUGNUMEROUNO DUDE "; trint_buf.show ();
        result = result + trint_buf;
        trint_buf = trInt();
        n++;
    }
    return result;
}

trInt trInt::operator / (const trInt &s_)
{
    trInt s = s_;
    trInt result;
    // уножаем делитель на 1 и T
    trInt a = s * trInt(-1); // то, что будем вычитать
    trInt b = s * trInt(1);
    trInt c = s* trInt(0);

    int n = 0; // позиция деления

    trInt ost;

    trInt buff;
    //buff.show ();

    int i = 0;

    buff = chop(s.digits ());

    //std::cout << "BUFF ::::: "; buff.show ();

    //trInt deb; deb = buff - s;std::cout << "BUFFBUFF ::::: "

    for(i = s.digits () + 1; i <= digits () + 1; i++)
    {
        // вычитаем а и b из первых (нескольких) цифр



        // если остаток от Т в ответе меньше, чем от еденицы, пишем его

        if(buff.toDouble () == 0) result.data.push_back (Trit('0'));
        else{
//            if(ost.toDouble () == s.toDouble () && i != digits () + 1)
//            {

//                result.data.push_back (Trit('T'));
//                ost = 0;
//            }
            if(abs((buff - c).toDouble ()) < abs((buff - a).toDouble ()) && abs((buff - c).toDouble ()) < abs((buff - b).toDouble ()))
            {
                result.data.push_back (Trit('0'));
                ost = buff;
            }
            else if(abs((buff - a).toDouble ()) < abs((buff - b).toDouble ())) // $$$ сделать сравненеи это легко
            {
                result.data.push_back (Trit('T'));
                ost = buff - a;
            }
            else
            {
                result.data.push_back (Trit('1'));
                ost = buff - b;
            }

//std::cout << "OST NOW :::::::: "; ost.show ();

            if(ost.toDouble () == s.toDouble ())
            {
                result.data.pop_back ();
                result.data.push_back (Trit('T'));
                ost = 0;
            }
            // buff теперь равен остатку с приписанной справа цифрой
            buff = ost;
        }

        if(i != digits () + 1) buff.data.push_back (data[i]);
       // std::cout << "BUFFER NOW IS "; buff.show ();
    }




    //std::cout << "THIS IS I " << i;

    return result;
}

bool trInt::operator == (const trInt &s)
{
    return data == s.data;
}

Trit trInt::getAt (int i) const
{
    return data.at (i);
}

void trInt::resign ()
{
    for(int i = 0; i < data.size (); i++)
    {
        Trit * t = &(data[i]);
        if(t->toChar () == 'T') t->set (2);
        else if(t->toChar () ==  '1') t->set (0);
    }
}

double trInt::toDouble () const
{
    double result = 0;

    for(int i = 0; i < data.size (); i++) // и здесь можно красивее пройти
        result += data[i].toInt() * (pow (3, digits () - i));

    return result;
}

void trInt::show ()
{
    for(Trit t : data)
        std::cout << t.toChar ();
    std::cout << std::endl;
}

int trInt::digits () const
{
    return data.size() - 1;
}

trInt trInt::chop (int n)
{
    QList<Trit> b;
    for(int i = 0; i <= n; i++) b.push_back (data.at (i));
    return trInt(b);
}
